package com.chengjue.kipeng;

public class AppVersion {
    private final static ThreadLocal<String> appVersion= new ThreadLocal<>();
    public static void setAppVersion(String version){
        appVersion.set(version);
    }
    public static String getAppVersion(){
        return appVersion.get();
    }

    public static void removeAppVersion(){
        appVersion.remove();
    }

}
