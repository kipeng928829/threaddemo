package com.chengjue.kipeng;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestVersion {
    public static void main(String[] args) {
        AppVersionRunnable appVersionRunnable1=new AppVersionRunnable("1");
        AppVersionRunnable appVersionRunnable3=new AppVersionRunnable("3");
        ExecutorService threadPool = Executors.newCachedThreadPool();
        threadPool.execute(appVersionRunnable1);
        threadPool.execute(appVersionRunnable3);


    }

    private static class AppVersionRunnable implements Runnable{
        private String version;

        public AppVersionRunnable(String version) {
            this.version = version;
        }

        @Override
        public void run() {
            AppVersion.setAppVersion(version);
            System.out.println("AppVersion:"+AppVersion.getAppVersion());
        }
    }

}
